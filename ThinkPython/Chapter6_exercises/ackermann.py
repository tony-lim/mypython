"""
File: ackermann.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: For ThinkPython exercises
"""


def ack(m, n):
    """see wikipedia - Ackermann function"""
    if m == 0:
        return n + 1
    if n == 0:
        return ack(m - 1, 1)
    return ack(m - 1, ack(m, n - 1))


print ack(3, 4)
for item in range(50):
    print ack(item + 1, item + 5),
