"""
File: test.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description:Just try to calculate the range of scores
"""

from random import randint

# There are 31 people in the class
# average score is 23.2
# My score is 19
# lowest is 9, highest is 40, cheuk got 13

def getrandom(x):
    if x == 0:
        return randint(9,40)
    else:
        return x

names = ["John Paul Abdou", "Lan Dang", "Janam Dave", "Eric Espinoza", "Oscar Gomez", "Nicholas Johnson", "Jacob Kosberg", "Alex Leyva", "Zhisheng Luo", "Allen Ma", "Gayane Malayan", "Zihao Meng", "Simerjit Nagra", "Joseph Park", "Kyungje Park", "Tony Lim", "Charles Sargent", "Kiran Sinjali", "Ryan Su", "Jeremiah Tam", "Cheuk Tsui", "Daniel Wang", "Ivan Wang", "Samer Wehbe", "Isali Win", "Dong Won", "Simon Xu", "Enoch Yeo", "Aram Yesayan", "Jia Yu", "Ethan"]
scores = [0 for i in range(31)]
scores[0] = 40
scores[1] = 9

for i in range(len(scores)):
    if "Tony" in names[i]:
        scores[i] = 19
    elif "Joseph" in names[i]:
        scores[i] = 34
    elif "Cheuk" in names[i]:
        scores[i] = 13

randomscores = [getrandom(i) for i in scores]
average = sum(randomscores)/float(len(randomscores))
print randomscores
print average
print sorted(randomscores)
peoplebelowme = sum([1 for x in randomscores if x < 19])
print 'peoplebelowme : ', peoplebelowme
print 'people above me : ', len(randomscores) - peoplebelowme - 1
