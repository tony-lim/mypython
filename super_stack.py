# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.


n = int(raw_input())
i = 0
stack = []
while i < n:
    prompt = raw_input().strip()
    inputs = prompt.strip().split()
    if inputs[0] == "push":
        stack.insert(0, int(inputs[1]) )
    elif inputs[0] == "pop":
        stack = stack[1:]
    elif inputs[0] == "inc":
        x   = int(inputs[1])
        for i in range(min(x,len(stack))):
            stack[-(i+1)] += 1
    i = i + 1
    # print 'stack: ', stack
    if len(stack) == 0:
        print 'EMPTY'
    else:
        print stack[0]


print 'done'
