import pygame, sys
from pygame.locals import *

pygame.init()

screen = pygame.display.set_mode((640,360),0,32)
color = (120,200,0)
color_t = (20,20,20)
position = (150,175)
position_2 = (450,175)
radius = (75)
radius_t = (5)

while True:

	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()

	screen.lock()
	pygame.draw.circle(screen,color,position,radius)
	pygame.draw.circle(screen,color_t,position,radius_t)
	pygame.draw.circle(screen,color,position_2,radius)
	pygame.draw.circle(screen,color_t,position_2,radius_t)
	screen.unlock()

pygame.display.update()


