bif = "bg.jpg"
mif = "red_arrow.png"

import pygame, sys
from pygame.locals import *

pygame.init()
screen = pygame.display.set_mode((1280,720),0,32)
background = pygame.image.load(bif).convert()
mouse_c = pygame.image.load(mif).convert_alpha()

while True:
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()
		if event.type == KEYDOWN:
			if event.key == K_LEFT:
				movex = -1
			elif event.key == K_RIGHT:
				movex = +1
			elif event.key == K_UP:
				movey = -1
			elif event.key == K_DOWN:
				movey = +1
	
	
	screen.blit(background, (0,0))
	x,y = pygame.mouse.get_pos()
	x -= mouse_c.get_width()/2
	y -= mouse_c.get_height()/2
	
	screen.blit(mouse_c,(x,y))
	pygame.display.update()
	

