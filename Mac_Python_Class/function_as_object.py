#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

def add(x, y):
    return x + y

def a_op_b(func, a, b):
    return func(a,b)

print a_op_b(add,2 ,3)

