#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.


# 1. Returning function in inner

def outer():
    def inner():
        print("I am the inner")
    return inner

foo = outer()
#foo()

# 2. Closure

x = "Outide outer"
def outer():
    x = "Inside outer"
    def inner():
        #x = "Inside inner"
        print(x)
    return inner

foo = outer()
foo()
