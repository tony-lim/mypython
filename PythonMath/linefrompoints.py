#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 atomic <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Just short script to find the equation of 2 lines from 2 points.
Code is taken from stackoverflow
"""

from numpy import ones,vstack
from numpy.linalg import lstsq

# points = [(1,5),(3,4)]
points = [(2,57),(4,83)]

x_coords, y_coords = zip(*points)

A = vstack([x_coords,ones(len(x_coords))]).T
m, c = lstsq(A, y_coords)[0]

print "Line Solution is y = {m}x + {c}".format(m=m,c=c)
