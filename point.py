from pylab import *

# for the points
distance = [x/100.0 for x in range(4,14)]
intensity = [5.30, 3.20, 2.10, 1.70, 1.40, 1.10, 0.90, 0.80, 0.70, 0.60]

x = map(log, distance)
y = map(log, intensity)


(m, b) = polyfit(x, y, 1)


equation = "%.2f x + %.2f" % (m, b)
print 'b : ', b
print 'm : ', m
print equation

yp = polyval([m, b], x)


# title
plt.title("Gauss Law Plots")

# plot here, in sequence
plot(x, yp)
scatter(x, y)

legend(['Linear fit plot', 'Data points'])

xlabel('d')
ylabel('I')


plt.text(x[1] + 0.5, y[1], equation)
show()
