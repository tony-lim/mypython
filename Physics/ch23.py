"""
File: ch23.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Testing taking derivative
"""

from sympy import *
import numpy as np
x = Symbol('x')
Ke = Symbol('Ke')
Q = Symbol('Q')
a = Symbol('a')
# y = x**2 + 1
y = (Ke * Q**2) / (x(2*a + x))
yprime = y.diff(x)
print yprime
print 2*x

#f = lambdify(x, yprime, 'numpy')
#f(np.ones(5))
