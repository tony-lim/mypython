"""
File: calculation.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github:  https://github.com/tony-lim
Description: Lorentz Force
"""


standard_value = 1.76E11
print 'standard value : ', standard_value

# e/m
def eoverm(V, R, I):
    B = 7.80*10**(-4) * I
    return 2*V/(R**2 * B**2)

def perError(a, b):
    """docstring for perError"""
    return 'percentage error : ', (b-a)/a*100

def processData(V, R, I):
    """docstring for processData"""
    print eoverm(V,R,I)
    print perError(eoverm(V,R,I), standard_value)

processData(220, 0.05, 1.18)
processData(222, 0.045, 1.30)
processData(250, 0.025, 2.42)
processData(240, 0.045, 1.36)
processData(280, 0.05, 1.36)
processData(340, 0.055, 1.36)
processData(340, 0.05, 1.60)
processData(288, 0.045, 1.62)
processData(260, 0.035, 1.84)
processData(480, 0.04, 2.44)
