from pylab import *

# for the planar
x = [x/100.0 for x in range(1,11)]
y = [6.5, 6.5, 6.5, 6.3, 6.3, 6.2, 6.2, 6.1, 5.8, 5.7]

(m, b) = polyfit(x, y, 1)

"""
This calls the polyfit function (that is in the pylab module).
Polyfit takes two variables and a degree. In this case the degree is 1 for a linear function.
The results goes to the two variables m (for the slope) and b for the y-intercept of the equation y = mx + b.
"""

equation = "%.2f x + %.2f" % (m, b)
print 'b : ', b
print 'm : ', m
print equation

yp = polyval([m, b], x)
"""
This just evaluates the polynomial with the coefficients [m,b] and value x.
So, for every x data point I have, this calculates a y value from the fitting function.
Now I have a new set of values yp.
"""

# title
plt.title("Gauss Law Plots")

# plot here, in sequence
plot(x, yp)
scatter(x, y)

legend(['Linear fit plot', 'Data points'])

xlabel('d')
ylabel('I')

# annotate('Equatioon', xy=(x[1], y[1], xytext=(0.01, 5.7), 
# 			arrowprops=dict(facecolor='black', shrink=0.05))
annotate(equation, xy=(x[1],y[1]), xytext=(0.01, 5.8),
            arrowprops=dict(facecolor='black', shrink=0.1),
            )

show()
