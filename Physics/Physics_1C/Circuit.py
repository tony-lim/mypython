"""
File: Circuit.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: Library for AC circuits functions
"""

import math as m

def Impedance(R, L, C, w):
    """Calculate Z(Impedance)
    """
    X_L = w*L
    X_C = 1/(w*C)
    return m.sqrt(R**2 + (X_L - X_C)**2)

def Irms(Vrms, Z):
    """TODO: Docstring for Irms.

    :Vrms: TODO
    :Z: TODO
    :returns: TODO

    """
    return Vrms/Z

def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    R = 1E3
    C = 1E-6
    L = 0.2
    V = 150
    w = 377
    Z = Impedance(R, L , C, w)
    I_rms = Irms(V, Z)
    print R, C, L
    print 'Z : ', Z
    print 'I_rms : ', I_rms

if __name__ == '__main__':
    main()
