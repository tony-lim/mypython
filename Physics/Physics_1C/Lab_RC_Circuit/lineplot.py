from pylab import *
import numpy as np


def calculateY(Vc):
    emf = 7.6 * 20
    return np.log(1 - (Vc/ emf ))


Vc_measured = [1.8, 3.5, 4.5, 5.4, 6.0, 6.5, 6.9, 7.2, 7.5, 7.6]
Vc = [i*20 for i in Vc_measured]
Y_calculated = [calculateY(_Vc) for _Vc in Vc]

print 'number data points' , len(Vc)
print 'Vc = ' , Vc
print 'Y_calculated = ' , Y_calculated

# for the planar
x = [2*t for t in range(1,11)]
y = Y_calculated

(m, b) = polyfit(x, y, 1)

"""
This calls the polyfit function (that is in the pylab module).
Polyfit takes two variables and a degree. In this case the degree is 1 for a linear function.
The results goes to the two variables m (for the slope) and b for the y-intercept of the equation y = mx + b.
"""

equation = "%.2f x + %.2f" % (m, b)
print 'b : ', b
print 'm : ', m
print equation

yp = polyval([m, b], x)
"""
This just evaluates the polynomial with the coefficients [m,b] and value x.
So, for every x data point I have, this calculates a y value from the fitting function.
Now I have a new set of values yp.
"""

# title
plt.title("Vc vs T")

# plot here, in sequence
plot(x, yp)
scatter(x, y)

legend(['Linear fit plot', 'Data points'])

xlabel('d')
ylabel('I')

show()
