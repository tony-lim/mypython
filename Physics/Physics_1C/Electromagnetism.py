# !/usr/bin/python
"""
File: Electromagnetism.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: Library to write all function written
"""

import scipy.integrate as integrate
import scipy.constants as sc
import math

permeability_0 = 4.0e-7*math.pi  # N/A^

def powerLost(I, rho, l, A):
    R = rho*l/2
    return I**2 * R


def getRadiusMagneticPerp(v, B, m = sc.electron_mass, q = sc.elementary_charge):
    """Calculate the radius of a particle with known v in a magnetic field"""
    return (m*v)/(q*B)


def testRadiusMagneticPerp():
    """test"""
    v = float(raw_input("v : "))
    B = float(raw_input("B : "))
    print 'v : ' , v
    print 'B : ' , B
    #print 'mass of lectron : ' ,sc.electron_mass
    #print 'charge of electron : ' ,sc.elementary_charge
    #print 'speed of light : ', sc.speed_of_light
    print 'radius : ', getRadiusMagneticPerp(v, B)
    print 'radius : ', getRadiusMagneticPerp(v, B, sc.proton_mass)

def magneticFLONGWIRE(I, d):
    """calculate magnetic field on a distance d away from the wire"""
    return permeability_0*I / (2*math.pi*d)

if __name__ == '__main__':
    print 'Debugging Electromagnetism functions'
    print 'This is Electromagnetism library'
