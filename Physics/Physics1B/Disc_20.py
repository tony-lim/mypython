import scipy.constants as sc
import math

mass_venus = 1.34 * 10**23
radius_venus = 2.575 * 10**6
oxgen_molarm = 0.032/sc.Avogadro #g/mol
T_venus = 95
g_accel = (sc.G * mass_venus)/(radius_venus**2)
v_esc = math.sqrt(2*g_accel*radius_venus)
v_rms = math.sqrt(3*sc.Bolzmann*95/oxgen_molarm)
v_rms2 = math.sqrt(3*sc.R*T_venus/0.032)

print 'Boltzman : ', sc.Bolzmann
print 'Gravitatonal : ', sc.G
print 'R : ', sc.R
print 'Mass venus : ', mass_venus
print 'Radius venus : ', radius_venus
print 'g on venus : ', g_accel
print 'Escape velocity : ', v_esc
print 'V_rms: ', v_rms
print 'v_rms2 : ', v_rms2
