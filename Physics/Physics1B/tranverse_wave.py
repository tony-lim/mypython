"""
File: tranverse_wave.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Finding the slope of 1/M and n^2
"""

from pylab import *


x = [14.2470437384, 9.9950024988, 8.3229296712, 6.6507049747, 4.0097838726]
y = [ 225, 144, 121, 100, 64 ]

(m,b) = polyfit(x,y,1)
print 'b : ', b
print 'm : ', m
print 'equation : ' ,str (m) + ' x + ' + str(b)

yp = polyval([m,b], x)

#title
plt.title("Simple  data plot")

#plot here, in sequence
plot(x,yp)
scatter(x,y)

legend(['Linear trend', 'Data Points'])

xlabel('x')
ylabel('y')

show()
