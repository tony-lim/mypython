#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

"""
"""

def testshiet(a):
    if a == "cpp":
            return str('h')
    else:
            return str('cpp')

a = 'h'

print testshiet(a)
