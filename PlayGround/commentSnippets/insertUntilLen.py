#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

string = "123"
string = " " + string + " "
star = "*"
while(len(string) < 78):
    string = star + string + star

#print string
print "*" * int((76 - len(string) / 2))
