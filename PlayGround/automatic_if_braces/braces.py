#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

def countCol(string):
    print string.count(';')

def processIntext(string, indentLevel):
    assert isinstance(string, str)
    string = string.replace("; ", ";")
    splittedBraces = string.split(';')
    splittedBraces = filter(None, splittedBraces) # remove empty elements
    splittedBraces = [" "*indentLevel + item + ";" for item in splittedBraces]
    return '\n'.join(splittedBraces)

def col_up(string):
    if string.count(';') > 1:
        return " {"
    else: return ""
def col_down(string):
    if string.count(';') > 1:
        return "}"
    else: return ""

theString = "line1"
#convertedString = convertBlock(theString)
#print convertedString

testLines = "cout << endl; x = 4;"
print 'test : ', testLines
#print 'braces : ', countCol(testLines)

#print col_up(testLines)
#print processIntext(testLines, 4)
#print col_down(testLines)
print testLines[-1:]
