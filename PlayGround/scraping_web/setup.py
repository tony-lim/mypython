# setup.py
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description' : 'Description of scraping_web',
    'author' : 'Tony Lim',
    'url' : 'https://github.com/atomic/scraping_web',
    'download_url' : 'https://github.com/atomic/scraping_web',
    'author_email' : 'atomictheorist@gmail.com',
    'version' : '0.1',
    'install_requires' : ['nose'],
    'packages' : ['scraping_web'],
    'scripts' : [],
    'name' : 'scraping_web'
    }

setup(**config)
# end-of-setup.py
