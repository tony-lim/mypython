# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

import requests
from bs4 import BeautifulSoup

home_url = "http://www.nba.com"
teams_url = "http://www.nba.com/teams"
teams_response = requests.get(teams_url) # return response object

#print type(teams_response)
#print teams_response.text

#students_url = "http://127.0.0.1:5000/students/"
#students_resp = requests.get(students_url)

#print students_resp.text

souped_teams = BeautifulSoup(teams_response.text)
print type(souped_teams)
list_of_links = souped_teams.find_all("a")
print type(list_of_links)

stats_url = []
for tag in list_of_links:
    if tag.get("href") and "/stats/" in tag.get("href"):
        stats_url.append(home_url + tag.get("href"))

print "\n".join(stats_url)
# Now, visit all URLs
TeamsResponses = [requests.get(TeamURL) for TeamURL in stats_url[0:4]]
SoupifiedTeams = [BeautifulSoup(TRes.text) for TRes in TeamsResponses]

for SPTeams in SoupifiedTeams:
    print " ---- Some teams \n"
    List_of_Tag = SPTeams.find_all("a") # not actually, just all tag with </a>
    for tag in List_of_Tag:
        if tag.get("href") and "playerfile" in tag.get("href"):
            print " - " + tag.string
