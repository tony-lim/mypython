# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tony Lim <atomictheorist@gmail.com>

#
# Distributed under terms of the MIT license.

def  braces(values):
    braces_or_not = []
    balance = True
    braceMap = { '[':']', '{': '}', '(':')' }
    for input in values:
        accu = ""
        balance = True
        for char in input:
            if char in braceMap.keys():
                accu += char
            elif char in braceMap.values():
                if len(accu) == 0 or (braceMap[ accu[-1] ] != char):
                    balance = False
                    break;
                else:
                    accu = accu[:-1]
        if accu == "" and balance:
            braces_or_not.append( "YES" )
        else:
            braces_or_not.append( "NO" )
    return braces_or_not


def isPresent (root,val):
    # write your code here
    # return 1 or 0 depending on whether the element is present in the tree or not
    if root.value == val:
        return 1
    else:
        if root.left and root.left:
            return isPresent(root.left) or isPresent(root.right)
        else:
            return 0

my_brace = [ "{}[]()", "{[}]}", "{[]}", "[{]]", "[a]", "{{{[[{[()]}]]}}}"]
my_brace2 =  [ "a", "}", "xx}" , "{"]
print braces(my_brace2)

# print 1 or 1
# print 0 or 0
# print 1 or 0
# print 0 or 1
