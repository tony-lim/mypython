"""
File: linear_regression.py
BY :  RHETT ALLAIN
Description: Linear regresson example
From: http://www.wired.com/2011/01/linear-regression-with-pylab/
"""

from pylab import *

x = [0.2, 1.3, 2.1, 2.9, 3.3]
y = [3.3, 3.9, 4.8, 5.5, 6.9]


(m,b) = polyfit(x,y,1)
"""
This calls the polyfit function (that is in the pylab module).
Polyfit takes two variables and a degree. In this case the degree is 1 for a linear function.
The results goes to the two variables m (for the slope) and b for the y-intercept of the equation y = mx + b.
"""
print 'b : ', b
print 'm : ', m
print str (m) + ' x + ' + str(b)

yp = polyval([m,b], x)
"""
This just evaluates the polynomial with the coefficients [m,b] and value x.
So, for every x data point I have, this calculates a y value from the fitting function.
Now I have a new set of values yp.
"""

#title
plt.title("Simple  data plot")

#plot here, in sequence
plot(x,yp)
scatter(x,y)

legend(['Linear trend', 'Data Points'])

xlabel('x')
ylabel('y')

show()
