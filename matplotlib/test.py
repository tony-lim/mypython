import matplotlib.pyplot as plt

myX = range(1, 5)
myY = range(1, 5)

for i,value in enumerate(myX):
    myX[i] = i*2


for i, value in enumerate(myY):
    myY[i] = i*4

print myX
print myY

plt.plot(myX, myY)
plt.ylabel('Sequency')
plt.show()

#plt.plot([1, 2,3,4])
#plt.ylabel('some numbers')
#plt.show()
