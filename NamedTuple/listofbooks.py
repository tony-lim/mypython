"""
File: listofbooks.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: Just a practice
"""

from collections import namedtuple

Book = namedtuple("book", "title author year price")

print type(Book)

gameofthrones = Book(title='game of thrones', author='grr martin', year='2009', price='20')
clashofkings = Book(title='clashofkings', author='grr martin', year='2009', price='30')
stormofswords = Book(title='storm of swords', author='grr martin', year='2009', price='40')

bookList = [gameofthrones, clashofkings, stormofswords]


print stormofswords
print stormofswords[0]
print stormofswords[1]
print bookList[0]

alltitle = [i[0] for i in bookList]
alltitle2 = bookList[:][0]
alltitle3 = zip(*bookList)[0]
print alltitle
print alltitle2
print alltitle3
print zip(*bookList)
