from sys import exit


def gold_room():
    print "This room is full of gold. How much do you take? (0-70)"
    prompt = raw_input("> ")
# Original
    # if "0" in prompt or "1" in prompt:
    # 	how_much = int(prompt)
    # else:
    # 	dead("Not a number")
# FIX 1: (Success)
    try:
        amount = int(prompt)
    except ValueError:
        dead("You failed algebra.")
    if amount < 50:
        print "Nice, you're not greedy, win!"
        exit(0)
    else:
        dead("You greedy bastard!")
# FIX 2: (Failed)
    # if prompt.isdigit():
    # how_much == int(prompt) #weird indention
    #    	if how_much < 50:
 #   			print "Nice, you're not greedy, win!"
 #   			exit(0)
    #    	else:
    #    		dead("You greedy bastard!")
    # else:
 #    	dead("You failed algebra")


def boss_fight():
    print "The dark lord oversaw your greediness."
    print "Thy come forth and summons skeleton."
    print "What are you going to do?"

    exit(0)


def bear_room():
    print "There is a bear here."
    print "The bear has a bunch of honey."
    print "The fat bear is in front of another door."
    print "How are you going to move the bear?"
    bear_moved = False

    while True:
        next = raw_input("> ")

        if next == "take honey":
            dead("The bear looks at you then slaps your face off.")
        elif next == "taunt bear" and not bear_moved:
# bear_moved = False, not False = True, "if ... and True."
# so, this command will exe for first time.
            print "The bear has moved from the door. You can go through it now."
            bear_moved = True
        elif next == "taunt bear" and bear_moved:
# bear_moved = True (if set True), command will exe.
            dead("The bear gets pissed off and chews your leg off.")
# improvise
        elif next == "open door" and not bear_moved:
            print "The bear is on your way."
# improvise
        elif next == "open door" and bear_moved:
            gold_room()
        else:
            print "I got no idea what that means."

            # so, bear_moved or not bear_moved gives condition to the situation.


def cthulu_room():
    print "Here you see the great evil Cthulu."
    print "He, it, whatever stares at you and you go insane."
    print "Do you flee for your life or eat your head?"

    next = raw_input("> ")

    if "flee" in next:
        start()
    elif "head" in next:
        dead("Well that was tasty!")
    else:
        cthulu_room()


def dead(why):
    print why, "You are dead!"
    exit(0)


def start():
    print "You are in a darm room."
    print "There is a door to your right and left."
    print "Which one do you take?"

    next = raw_input("> ")
    if next == "left":
        bear_room()
    elif next == "right":
        cthulu_room()
    else:
        dead("You stumble around the room until you starve.")
gold_room()
