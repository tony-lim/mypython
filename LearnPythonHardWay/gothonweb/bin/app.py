# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2015 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.

import web

urls = (
    '/hello', 'index',
)

app = web.application(urls, globals())
render = web.template.render('templates/', base="layout")

class index(object):
    def GET(self):
        return render.hello_form()

    def POST(self):
        form = web.input(name="Nobody", greet="Hello")
        greeting = "%s, %s" % ( form.greet, form.name )
        return render.index(greeting = greeting)

if __name__ == '__main__':
    app.run()
