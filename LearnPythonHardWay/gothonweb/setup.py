# setup.py
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description' : 'Description of gothonweb',
    'author' : 'Tony Lim',
    'url' : 'https://github.com/atomic/gothonweb',
    'download_url' : 'https://github.com/atomic/gothonweb',
    'author_email' : 'atomictheorist@gmail.com',
    'version' : '0.1',
    'install_requires' : ['nose'],
    'packages' : ['gothonweb'],
    'scripts' : [],
    'name' : 'gothonweb'
    }

setup(**config)
# end-of-setup.py
