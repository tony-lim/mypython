import random
people = random.randint(30, 60)
cars = random.randint(8, 14)
buses = random.randint(4,7)

cars_available_seats = cars*5 		#1 car fits 5 people
buses_available_seats = buses*10 	#1 buses fits 10 people

print "%r people\n%r cars\n%r buses." %(people,cars,buses)

if cars > people:
	print "We should take the cars."
elif cars < people:
	print "We should not take cars."
else:
	print "We can't decide."

if buses > cars:
	print "That's too many buses."
elif buses < cars:
	print "Maybe we could take the buses."
else:
	print "we still can't decide."

if people > buses:
	print "Alright, let's just take the buses."
else:
	print "Fine, let's stay home then."

#YOU MODIFIED THE VARIABLE, for fun.
#YOU CREATED NEW MORE COMPLICATED VERSION IN ec30.py