i = 0
numbers = []

while i < 6:
    print "At the top i is %d" % i
    numbers.append(i)  # first loop : add 0
                                                            # second loop : add
                                                            # 1

    i = i + 1
    # first loop: only 0, because append(i) add 0 the first loop?
    print "Numbers now: ", numbers
    # this print 1 for first i, but remember this "i" is after the append(i)
    # of 0
    print "At the bottom i is %d" % i


print "The numbers: "

for num in numbers:
    print num
