from sys import argv
from os.path import exists

script, from_file, to_file = argv

print "Copying from %s to %s" % (from_file, to_file)

# we could do these two on one line too, how?

indata = open(from_file).read()  # revised 1 line

print "the input file is %d bytes long \n Does the output file exist? %r" % (len(indata), exists(to_file))
        # If the file does not exist, FALSE. it will create new file.

print "Ready, hit RETURN to continue, CTRL-C to abort."
raw_input()

output = open(to_file, 'w')
output.write(indata)

print "Alright, all done."

output.close()
