#!/usr/bin/env python -tt
import random
def intro():
        myName = random.choice(["Bob", "The Mighty One", "Fluffy Bunny"])
        print "Hello my name is: " + myName
        print "What is your Name?"
        userName = raw_input()
        print "Ahh, it's nice to meet you, " + userName
        print "Let's play a game!"
        game()
def game():
        print "Guess what fruit I am thinking of!"
        fruit = random.choice(["orange", "apple", "pear"])
        answer = raw_input()
        if answer != fruit:
          print "Wrong. " + "I was thinking of", fruit + "."
          game()
        else:
          print "Correct."  
def main():
  intro()
if __name__ == "__main__":
  main()