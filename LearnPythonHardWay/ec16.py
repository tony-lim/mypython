from sys import argv

script, filename = argv

print "We're going to erase %r." % filename
print "If you don't want that, hit CTRL-C (^C)."
print "If you do want that, hit RETURN."

raw_input("?")

print "Opening the file . . . "
# open (filename, 'w') 'w' is for write only, 'r' is for read, 'w+' for both.
target = open(filename, 'w')
                                                        # or just leavef it
                                                        # blank?

print "Truncating the file. Goodbye!"
target.truncate()

print "Now I'm going to ask you for three lines."

line1 = raw_input("line 1: ")
line2 = raw_input("line 2: ")
line3 = raw_input("line 3: ")

print "I'm going to write these to the file."

# this is for python 3, fortunately available in 2.6
target.write("{0}\n{1}\n{2}\n".format(line1, line2, line3))
target = open(filename).read()
# it looks like when you open  a file in one mode ('r','w',etc), it
# disables other mode.
print "And, read the file."
print target


print "And finally, we close it."
                        # Here we don't need to use "target.close()", check stack overflow or just figure it out.
                        # autoclose for calling function?
