from sys import argv

script, filename = argv

txt = open(filename)

print "Here's your file %r:" %filename
print txt.read()

print "I'll also ask you to type it again:"

txt_again = open(raw_input ("> "))

print txt_again.read()

#this is another example from extra credit, using file and method attached.
file1 = file(raw_input ("> "))
print file1.read()

txt.close()
file1.close()
txt_again.close()


#you created another txt file "test1". 
#using the first input as test1 and secondi input as ex15_sample.txt seems to work.