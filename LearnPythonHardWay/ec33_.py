i = int(raw_input("i starts at "))
x = int(raw_input("Increase by 1 as long as i < "))
step = int(raw_input("Step = "))

numbers = []

while i < x:
	print "At the top i is %d" %i
	numbers.append(i)

	i = i + step
	print "Numbers now: ", numbers
	print "At the bottom i is %d" %i


print "The numbers: "

for num in numbers:
	print num