"""
File: ex11.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: exercise 11
"""

print "How old are you?",
age = raw_input('My age is ')  # initially its only raw_input(), you modify.
print "How tall are you?",
height = raw_input("My height is ")
print "How much do you weigh?",
weight = raw_input("My weight is ")

print "So, you\'re %s old, %s tall and %s heavy." % (age, height, weight)
