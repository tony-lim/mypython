import random

people = random.randint(10,15) 			#modified for extra credit
cats = random.randint(15, 30) - people 	#you looked up code from stackoverflow
dogs = random.randint(5,10 ) + int(people/2)

if people < cats:
	print "Too many cats! The world is doomed!"

if people > cats: #which is wrong, so the command do nothing
	print "Not many cats! The world is saved" 

if people == cats:
	print "Cats conspiracy?!"

if people < dogs: #wrong, do nothing
	print "The world is drooled on!"

if people > dogs: #true, execute cmd
	print "The world is dry!"

dogs += random.randint(-1, 5)

if people >= dogs:	#true, exe
	print "People are greater than equal to dogs."

if people <= dogs: 	#true, exe
	print "People are less than or equal to dogs."

if people == dogs:	#true, exe
	print "Dogs conspiracy."

if (people < dogs and people < cats):
	print "Humanity is ruined."
if (people < dogs and people < cats) and cats == dogs:
	print "They conspire against us ?!"

if (people > dogs and people > cats):
	print "Bright future awaits."

print "People: %r\nCats:%r\nDogs:%r." %(people,cats,dogs)

# 1. if put condition for next command
# 2. the indent set the command under if condition
# 3. if it isnt indented, the command will execute by itself
# 4.

def func_1(x):	
	y = x**2 + (3*x) + 5
	if x == 0:
		print "y function has %r value when x is zero" %y
	elif x >0:
		print "y has value of %r when x is more than zero" %y
	elif x <0:
		print "y has value of %r when x is less than zero" %y
	else:
		print "y is a mystery"

func_1(5)
func_1(0)
func_1(-5)

#5. extra credit 5 modify the top variable (heavily modified, LOL)