# setup.py
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description' : 'Description of ex47',
    'author' : 'Tony Lim',
    'url' : 'https://github.com/atomic/ex47',
    'download_url' : 'https://github.com/atomic/ex47',
    'author_email' : 'atomictheorist@gmail.com',
    'version' : '0.1',
    'install_requires' : ['nose'],
    'packages' : ['ex47'],
    'scripts' : [],
    'name' : 'ex47'
    }

setup(**config)
# end-of-setup.py
