a = 0#int(raw_input("a = "))
b = 15#int(raw_input("b = "))
c = 3#int(raw_input("c = "))

test = []
test_ = []

for i in range(a,b,c):
	test.append(i)
	print test					#REMEMBER THIS IS A LOOP.
								#The loop repeats test.append + print and stops at assigned range.
for i in test:
	test.pop()
	print test

for i in range(a,b,c):
	test_.append(i)
	print test_

for i in range(a,b,c):
	test_.remove(i)
	print test_

for i in test:
	print "test : %r" %i 		#leaves 2 values, shouldnt be.

for i in test_:
	print "test_ : %r" %i