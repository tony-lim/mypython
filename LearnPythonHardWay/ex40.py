"""
File: ex40.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: Object and Classes
"""


class Song(object):
    """docstring for Song"""
    def __init__(self, lyrics):
        super(Song, self).__init__()
        self.lyrics = lyrics

    def sing_me_a_song(self):
        for line in self.lyrics:
            print line


def main():
    """TODO: Docstring for main.
    :returns: TODO

    """
    happy_bday = Song(["Happy birthday to you",
                       "I don't want to get sued",
                       "So I'll stop right there"])

    bulls_on_parade = Song(["They rally around the family",
                            "With pockets full of shells"])

    happy_bday.sing_me_a_song()
    bulls_on_parade.sing_me_a_song()

if __name__ == '__main__':
    main()
