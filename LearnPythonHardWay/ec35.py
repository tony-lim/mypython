import random
import time
import sys


def slow_t(sentence):
    for i in sentence:
        sys.stdout.write(i)
        sys.stdout.flush()
        time.sleep(0.03)
        time.sleep(0.5)


def slow_t2(sentence):
    for i in sentence:
        sys.stdout.write(i)
        sys.stdout.flush()
        time.sleep(0.6)
        time.sleep(0.5)


def gold_room():
    print "This room is full of gold. How much do you take? (0-70)"
    prompt = raw_input("> ")
    if "0" in prompt or "1" in prompt:
        how_much = int(prompt)
    else:
        dead("Not a number")

    if how_much < 50:
        print "Nice, you're not greedy, win!"
        exit(0)
    else:
        boss_fight()


def boss_fight():
    slow_t("The dark lord oversaw your greediness.\n")
    slow_t("Thy come forth and summons skeleton.\n")
    slow_t("What are you going to do?\n\n")

    weakness = False
    boss_hp = random.randint(1700, 2000)

    while True:
        print "Boss Health = ",
        slow_t2(str(boss_hp))
        print "\n"
        if boss_hp < 1000:
            print "The dark lord looks panting."
            weakness = True

        act = raw_input("Action (attack,ultimate,run)> ")
        if act == "attack":
            dmg = random.randint(100, 300)
            if dmg >= 250:
                dmg += 200
                boss_hp -= dmg
                print "You deal %r critical damage" % dmg
            elif dmg < 250:
                boss_hp -= dmg
                print "You deal %r damage" % dmg
            elif act == "ultimate" and weakness:
                slow_t("You used your ultimate and the dark lord has fallen.\n")
                slow_t("Congratulations on winning the game.")
                exit(0)
            elif act == "ultimate" and not weakness:
                print "Your ultimate is not effective."
            elif act == "run":
                dead(
                    slow_t(
                        "Your soul has been consumed when you turn your back."))
                time.sleep(1)



def bear_room():
    print "There is a bear here."
    print "The bear has a bunch of honey."
    print "The fat bear is in front of another door."
    print "How are you going to move the bear?"
    bear_moved = False

    while True:
        next = raw_input("> ")

        if next == "take honey":
            dead("The bear looks at you then slaps your face off.")
        # bear_moved = False, not False = True, "if ... and True."
        elif next == "taunt bear" and not bear_moved:
            # so, this command will exe for first time.
            print "The bear has moved from the door. You can go through it now."
            bear_moved = True
        # bear_moved = True (if set True), command will exe.
        elif next == "taunt bear" and bear_moved:
            dead("The bear gets pissed off and chews your leg off.")
            # improvise
        elif next == "open door" and not bear_moved:
            print "The bear is on your way."
            # improvise
        elif next == "open door" and bear_moved:
            gold_room()
        else:
            print "I got no idea what that means."

            # so, bear_moved or not bear_moved gives condition to the situation.


def cthulu_room():
    print "Here you see the great evil Cthulu."
    print "He, it, whatever stares at you and you go insane."
    print "Do you flee for your life or eat your head?"

    next = raw_input("> ")

    if "flee" in next:
        start()
    elif "head" in next:
        dead("Well that was tasty!")
    else:
        cthulu_room()


def dead(why):
    time.sleep(0.5)
    print "You are dead!"
    exit(0)


def start():
    print "You are in a darm room."
    print "There is a door to your right and left."
    print "Which one do you take?"

    next = raw_input("> ")
    if next == "left":
        bear_room()
    elif next == "right":
        cthulu_room()
    else:
        dead("You stumble around the room until you starve.")

gold_room()
