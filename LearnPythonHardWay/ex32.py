the_count = [1,2,3,4,5]
fruits = ['apple','oranges','pears','apricots']
change = [1, 'pennies', 2, 'dimes', 3, 'quarters']

#this first kind of for-loop goes through a list
for number in the_count:
	print "This is count %d" %number

#same as above
for fruit in fruits:
	print "A fruit of type :%s" %fruit

#also we can go through mixed lists too
#notice we have to use %r since we don't know what's in it
for i in change:
	print "I got %r" %i

#we can also build lists, first start with an empty one
elements = []
tests = []


#then use the range function to do 0 to 5 counts
for i in range(0, 6):
	print "Adding %d to the list." %i
	# append is a function that lists understand
	elements.append(i)

# now we can print them out too
for i in elements:
	print "elements was: %d" %i

for i in tests:
	print "Test number: %r"%i # This one does not work.

tests_ = range(3,9,2) #skipped the for-loop
tests_2= range(0,8,2)
for i in tests_2:
	print "Tests_ (%r,%r)" %(i,i)

# Extra credit 
#2. avoid, maybe, but to print it out line by line, we have to call "i".
#3. besides append.
# list.append(x)
# Add an item to the end of the list; equivalent to a[len(a):] = [x].

# list.extend(L)
# Extend the list by appending all the items in the given list; equivalent to a[len(a):] = L.

# list.insert(i, x)
# Insert an item at a given position. The first argument is the index of the element before which to insert, so a.insert(0, x) inserts at the front of the list, and a.insert(len(a), x) is equivalent to a.append(x).

# list.remove(x)
# Remove the first item from the list whose value is x. It is an error if there is no such item.

# list.pop([i])
# Remove the item at the given position in the list, and return it. If no index is specified, a.pop() removes and returns the last item in the list. (The square brackets around the i in the method signature denote that the parameter is optional, not that you should type square brackets at that position. You will see this notation frequently in the Python Library Reference.)

# list.index(x)
# Return the index in the list of the first item whose value is x. It is an error if there is no such item.

# list.count(x)
# Return the number of times x appears in the list.

# list.sort()
# Sort the items of the list, in place.

# list.reverse()
# Reverse the elements of the list, in place.