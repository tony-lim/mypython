print "T",True and True 
print "F",False and True 
print "F",1 == 1 and 2 == 1 
print "T","test" == "test" 
print "T",1 == 1 or 2 != 1 
print "T",True and 1 == 1 
print "F",False and 0 != 0 
print "T",True or 1 == 1 
print "F","test" == "testing" 
print "T",1 != 0 and 2 == 1 ,"wrong"	#you got this one wrong, its true and false, false
print "T","test" != "testing" 
print "F","test" == 1 
print "T",not (True and False) 
print "F",not (1 == 1 and 0 != 1) 
print "F",not (10 == 1 or 1000 == 1000) 
print "F",not (1 != 10 or 3 == 4) 
print "T",not ("testing" == "testing" and "Zed" == "Cool Guy") 
print "T",1 == 1 and not ("testing" == 1 or 1 == 0) 
print "F","chunky" == "bacon" and not(3 == 4 or 3 == 3) 
print "F",3 == 3 and not("testing" == "testing" or "Python" == "Fun") 
