def cheese_and_crackers(cheese_count, boxes_of_crackers):
	print "You have %d cheeses!" %cheese_count
	print "You have %d boxes of crackers!" %boxes_of_crackers
	print "Man that's enough for a party!"
	print "Get a blanket.\n"

def	numerical_1(a1, b1 ,c1):
	x = a1 + b1 + c1
	y = a1 * b1 * c1
	print "%r and %r" %(x,y)
	
print "here goes numbers."
numerical_1(1,2,3)

#This combines the raw_input from user.
a = int(raw_input("a1 :"))
b = int(raw_input("b1 :"))
c = int(raw_input("c1 :"))
numerical_1(a,b,c)

print "We can just give the function numbers directly:"
cheese_and_crackers(20, 30)

print "OR, we can use variables from our script: "
x = 10  			#change this to X and Y, for your experiment
y = 50				#i think you are familiar with this concept in mathstudio

cheese_and_crackers (x, y)

print "We can even do math inside too: "
cheese_and_crackers(10+20,5+6)


print "And we can combine the two, variable and math:"
cheese_and_crackers(x + 100, y + 1000)		#this too. X , Y
