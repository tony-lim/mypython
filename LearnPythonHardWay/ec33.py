

def loop(a, b, c):
    numbers = []
    while a < b:
        print "At the top i is %d" % a
        numbers.append(a)  # first loop : add 0
                                                                # second loop :
                                                                # add 1

        a = a + c
        print "Numbers now: ", numbers
        print "At the bottom i is %d" % a

    print "The numbers: "

    for num in numbers:
        print num

# To run this code, use python, and "from ec33 import *"
# type "loop(start,end,step)"


def loop2(a, b, c):
    numbers = []
    print "Start = %d" % a
    print "Steps = %d" % c
    while a != b:
        print "Current : ", numbers
        a += c
        print "Next number to add into the seq. : %d" % a
        numbers.append(a)
        print "Processed : ", numbers

    print "The numbers: "

    for num in numbers:  # This used to print the list of numbers vertically.?
        print num
    print numbers


def loop3(a, b, c):
    numbers = []
    print "Start = %d" % a
    print "Steps = %d" % c
    for i in range(a, b, c):
        print "Current : ", numbers
        numbers.append(i)
        print "Processed : ", numbers

    print "The numbers: "

    for num in numbers:  # This used to print the list of numbers vertically.?
        print num
    print numbers


def loop4(a, b):  # get rid of increment, default steps = 1
    numbers = []
    print "Start = %d" % a
    for i in range(a, b):
        print "Current : ", numbers
        numbers.append(i)
        print "Processed : ", numbers

    print "The numbers: "

    for num in numbers:  # This used to print the list of numbers vertically.?
        print num
    print numbers
