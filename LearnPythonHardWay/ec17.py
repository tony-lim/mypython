from sys import argv
from os.path import exists

script, from_file, to_file = argv

print "Copying from %s to %s" %(from_file, to_file)

#we could do these two on one line too, how?

output = open(to_file, 'w')
output.write(open(from_file).read())  #SUPER SHORT ONE-LINER , from stackoverflow

print "Alright, all done."

output.close()
