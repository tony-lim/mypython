tabby_cat = "\tI'm tabbed in."
persian_cat = "I'm split\non a line."
backslash_cat = "I'm \\ a \\ cat."

#original exercise 
fat_cat = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
"""

#this is the one line version of the script
fat_cat = "I'll do alist:\n\t* Cat food \n\t* Fishies\n\t* Catnip\n\t* Grass"

#this is more complex version (combined with format string)
fat_cat = "I'll do alist:\n\t* %r\n\t* %s\n\t* %r\n\t* %s" %('Cat food',"Fishies","Catnip","Grass")


print tabby_cat
print persian_cat
print backslash_cat
print fat_cat