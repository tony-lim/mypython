"""
File: problem5.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: Iterative string loop
"""


def laceStrings(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length,
    then the extra elements should appear at the end.
    """
    # Your Code Here
    # maxLength = (len(s1) if len(s1) > len(s2) else len(s2))
    L1 = list(s1)
    L2 = list(s2)
    maxLength = max(len(L1), len(L2))
    myString = ""
    for i in range(maxLength):
        try:
            myString += L1[i]
        except:
            pass
        try:
            myString += L2[i]
        except:
            pass
    return myString


def laceStringsRecur(s1, s2):
    """
    s1 and s2 are strings.

    Returns a new str with elements of s1 and s2 interlaced,
    beginning with s1. If strings are not of same length,
    then the extra elements should appear at the end.
    """
    def helpLaceStrings(s1, s2, out):
        if s1 == '':
            return out + s2
        if s2 == '':
            return out + s1
        else:
            return helpLaceStrings(s1[1:], s2[1:], out + s1[0] + s2[0])
    return helpLaceStrings(s1, s2, '')


def main():
    s1 = "xxx"
    s2 = "efgh"
    # print laceStrings(s1, s2)
    print laceStringsRecur(s1, s2)


if __name__ == '__main__':
    main()
