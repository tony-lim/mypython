"""
File: problem4.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/tony-lim
Description: MIT quiz 1 problem 4( first problem)
"""

def myLog(x, b):
    '''
    x: a positive integer
    b: a positive integer; b >= 2

    returns: log_b(x), or, the logarithm of x relative to a base b.
    '''
    # Your Code Here
    power = 1
    while(b**power <= x):
        power += 1
    return power - 1


def main():
    x = None
    b = None
    while (True):
        x = int(raw_input("x : "))
        b = int(raw_input("b : "))
        print "Log(%d,%d) is %d" % (x, b, myLog(x, b))

if __name__ == '__main__':
    main()
