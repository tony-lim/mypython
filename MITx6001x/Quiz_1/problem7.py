"""
File: problem7.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/atomic
Description: combinatorics problem
"""


def McNuggets(n):
    """
    n is an int

    Returns True if some integer combination of 6, 9 and 20 equals n
    Otherwise returns False.
    """
    a, b, c = 0, 0, 0

    def result():
        return 6*a + 9*b + 20*c

    while (c*20 <= n):
        b = 0
        while (b*9 <= n):
            a = 0
            while (a*6 <= n):
                if result() == n:
                    return True
                a += 1
            b += 1
        c += 1
    return False

for i in range(60):
    if(McNuggets(i)):
        print "%d is True" % (i)
