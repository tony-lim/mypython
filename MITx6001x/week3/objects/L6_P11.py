def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.
    returns: The key with the largest number of values associated with it
    '''
    # Your Code Here
    maxLen = 0
    max = ''
    for key, value in aDict.items():
        if len(value) > maxLen:
            maxLen = len(value)
            max = key
    if maxLen is 0:
        return None
    return max

animals = {'a': ['aardvark'], 'b': ['baboon'], 'c': ['coati']}
animals['d'] = ['donkey']
animals['d'].append('dog')
animals['d'].append('dingo')
print animals
print animals.items()

print biggest(animals)
