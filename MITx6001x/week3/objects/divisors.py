## divisors

def findDivisors(n1, n2):
    """assumes that n1 and n2 are positive ints
       returns a tuple containing the common divisors of n1 and n2"""
    divisors = () # the empty tuple
    for i in range(1, min(n1, n2) + 1):
        if n1%i == 0 and n2%i == 0:
            divisors = divisors + (i,)
            print ' divisors : ', divisors
    return divisors

def testTupleDynamics(someTuples):
    """docstring for testTupleDynamics"""
    # change the tuple
    for item in enumerate(someTuples):
        someTuples += (101,)
    print someTuples

divisors = findDivisors(20, 100)
total = 0
for d in divisors:
    total += d
print(total)
divisors = testTupleDynamics(divisors)
print divisors
