def oddTuples(aTup):
    """
        aTup: a tuple
        returns: tuple, every other element of aTup.
    """
    # Your Code Here
    #oddTup = ()
    #for i in enumerate(aTup):
        #if i[0] % 2 == 0:
            #oddTup += (i[1],)
    #return oddTup
    return aTup[::2]


testTuple = ("a", "asd", [1, 2, 3])
print oddTuples(testTuple)
