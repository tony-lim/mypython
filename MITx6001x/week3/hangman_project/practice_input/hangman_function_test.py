import string


def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    ex: isWordGuessed('apple', ['p','e'])
        return False
    ex: isWordGuessed('apple', ['a','l',p','e'])
        return True
    '''
    for item in secretWord:
        if item not in lettersGuessed:
            return False
    return True

secretWord = 'apple'
lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
print isWordGuessed(secretWord, lettersGuessed)


def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
      ex: getGuessedWord('apple', 'pe')
      return: _pp_e
    '''
    # FILL IN YOUR CODE HERE...
    GuessedWord = ''
    for char in secretWord:
        if char not in lettersGuessed:
            GuessedWord += '_'
        else:
            GuessedWord += char
    return GuessedWord

print getGuessedWord(secretWord, lettersGuessed)


def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE...
    availableLetters = ''
    for char in string.ascii_lowercase:
        if char not in lettersGuessed:
            availableLetters += char
    return availableLetters

lettersGuessed = ['e', 'i', 'k', 'p', 'r', 's']
print getAvailableLetters(lettersGuessed)
lettersGuessed += 'x'
print lettersGuessed
lettersGuessed += 'x'
print lettersGuessed
