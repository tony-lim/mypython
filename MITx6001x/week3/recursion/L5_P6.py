def lenIter(aStr):
    '''
    aStr: a string
    returns: int, the length of aStr
    '''
    # Your code here
    len = 0
    for item in aStr:
        len += 1
    return len
