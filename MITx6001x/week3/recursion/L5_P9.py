"""
File: L5_P9.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: reverse palindromes (semordnilap)
"""


def semordnilapWrapper(str1, str2):
    # A single-length string cannot be semordnilap
    if len(str1) == 1 or len(str2) == 1:
        return False
    # Equal strings cannot be semordnilap
    if str1 == str2:
        return False
    return semordnilap(str1, str2)


def semordnilap(str1, str2):
    '''
    str1: a string
    str2: a string
    returns: True if str1 and str2 are semordnilap;
             False otherwise.
    '''
    # Your code here
    if len(str1) != len(str2):
        return False
    elif len(str1) == 1 and len(str2) == 1:
        if str1[0] == str2[0]:
            return True
        else:
            return False
    elif str1[0] == str2[-1]:
        return semordnilap(str1[1:], str2[:-1])

a = 'racecar'
b = a[::-1]
print 'a : ', a
print 'b : ', b
print 'smordnilap a,b : ', semordnilap(a, b)
