"""
File: L5_P4.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description:

Write an iterative function, gcdIter(a, b),
that implements this idea. One easy way to do this is to begin with a test
value equal to the smaller of
the two input arguments,
and iteratively reduce this test value by 1 until you either reach a case where the test divides
both a and b without remainder, or you reach 1.
"""


def smaller(a, b):
    """docstring for smaller"""
    if a > b:
        return b
    else:
        return a


def gcdIter(a, b):
    '''
    a, b: positive integers
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # your code here
    gcd = smaller(a, b)
    while not (a % gcd == 0 and b % gcd == 0):
        gcd -= 1
    return gcd

a = int(raw_input(' a : '))
b = int(raw_input(' b : '))
print gcdIter(a, b)
