
def iterPower(base, exp):
    '''
    (base, exp)
    base: int or float.
    exp: int >= 0

    returns: int or float, base^exp
    '''
    # Your code here
    result = abs(base)
    if (exp == 0):
        return 1
    if(base < 0):
        isNegative = True
        base = abs(base)
    else:
        isNegative = False
    for exp in xrange(exp-1):
        result *= base
    #while (exp > 1):
        #result *= base
        #exp -= 1
    if (isNegative and (exp % 2 == 1)):
        result *= -1
    return result

print '2^3 = '
print iterPower(2, 3)
print iterPower(-2, 3)
