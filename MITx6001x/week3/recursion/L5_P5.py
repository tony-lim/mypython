"""
File: L5_P5.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: GCD using euclid recursive algorithm
"""

def gcdRecur(a, b):
    '''
    a, b: positive integers
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    # Your code here
    if b == 0:
        return a
    return gcdRecur(b, a % b)
