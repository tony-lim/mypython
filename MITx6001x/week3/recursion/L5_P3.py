"""
File: L5_P3.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: another way to computer exp of a base
"""

totalLoop = 0


def recurPowerNew(base, exp):
    '''
    base: int or float.
    exp: int >= 0
    returns: int or float, base^exp
    '''
    # Your code here
    global totalLoop
    totalLoop += 1
    if exp == 0:
        return 1
    elif exp % 2 == 0:
        return recurPowerNew(base*base, exp/2)
    return base*recurPowerNew(base, exp-1)

base = float(raw_input('input your base: '))
exp = int(raw_input('input your exp: '))
print recurPowerNew(base, exp)
print 'total recursive call = ', totalLoop
