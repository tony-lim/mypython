"""
File: L5_P7.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: recursive find length of string
"""
def lenRecur(aStr):
    '''
    aStr: a string
    returns: int, the length of aStr
    '''
    # Your code here
    if aStr == '':
        return 0
    return 1 + lenRecur(aStr[1:])
