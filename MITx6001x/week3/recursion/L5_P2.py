totalLoop = 0


def recurPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0
    returns: int or float, base^exp
    '''
    # Your code here
    global totalLoop
    totalLoop += 1
    if exp == 0:
        return 1
    return base*recurPower(base, exp-1)

base = float(raw_input('input your base: '))
exp = int(raw_input('input your exp: '))
print recurPower(base, exp)
print 'total recursive call = ', totalLoop
