"""
File: radiation_exposure.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Mit week 3 problem
"""


def f(x):
    import math
    return 60*math.e**(math.log(0.5)/55.6 * x)


def radiationExposure(start, stop, step):
    '''
    Computes and returns the amount of radiation exposed
    to between the start and stop times. Calls the
    function f (defined for you in the grading script)
    to obtain the value of the function at any point.

    start: integer, the time at which exposure begins
    stop: integer, the time at which exposure ends
    step: float, the width of each rectangle. You can assume that
      the step size will always partition the space evenly.

    returns: float, the amount of radiation exposed to
      between start and stop times.
    '''
    #domains = []
    #iterator = start
    #while(iterator != stop):
        #domains.append(iterator)
        #iterator += step
    #range = map(f, domains)
    #range = [y*step for y in range]
    #return sum(range)
    # alternative approach
    total = 0
    iterator = start
    while(iterator < stop):
        total += f(iterator)*step
        iterator += step
    return total

print radiationExposure(600, 1200, 5)
