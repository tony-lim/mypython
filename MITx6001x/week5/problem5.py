"""
File: problem5.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/atomic
Description: Comparing sort
"""


def search(L, e):
    for i in range(len(L)):
        if L[i] == e:
            return True
        if L[i] > e:
            return False
    return False


def newsearch(L, e):
    size = len(L)
    for i in range(size):
        if L[size-i-1] == e:
            return True
        if L[i] < e:
            return False
        return False


A = range(2)
B = range(2)

test = 1
print 'lookng for ', test
print search(A, test)
print newsearch(A, test)
print '\n'
# print search(B, test)
# print newsearch(B, test)
