def binary_search(L, e):
    """ Binary search re-implementation from MIT course
        given the list is sorted"""
    def recurSearch(e, L, low, high):
        """recursive call of this function to chop down the element space"""
        mid = int( (low + high)/2)
        if high == low:
            return L[low] == e
        elif e == L[mid]:
            return True
        elif e > L[mid]:
            return recurSearch(e, L, mid + 1, high)
        elif e < L[mid]:
            return recurSearch(e, L, 0, mid - 1)
    if e < L[0]:
        return False
    else:
        return recurSearch(e, L, 0, len(L) - 1)


def binary_search2(L, e):
    """ Binary search re-implementation from MIT course
        given the list is sorted, 
        this method will shrink the list instead of the high and low"""
    high = len(L) - 1
    mid = high/2
    if high == 0:
        return L[0] == e
    if L[mid] == e:
        return True
    elif L[mid] > e:
        return binary_search2(L[0:mid], e)
    elif L[mid] < e:
        return binary_search2(L[mid+1:], e)

    L = [0,1,2,4,6,8]
print binary_search(L, 7)
print binary_search2(L, 7)