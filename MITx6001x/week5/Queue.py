"""
File: Queue.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/atomic
Description: Implement a queue class
"""


class Queue(object):

    """Queue data structure"""

    def __init__(self):
        """TODO: to be defined1. """
        self.myQueue = []

    def insert(self, e):
        self.myQueue.append(e)

    def remove(self):
        if not self.myQueue:
            raise ValueError("ValueError")
        i = self.myQueue[0]
        del self.myQueue[0]
        return i

queue = Queue()
queue.insert(5)
queue.insert(6)
print queue.remove()
queue.insert(7)
print queue.remove()
print queue.remove()
