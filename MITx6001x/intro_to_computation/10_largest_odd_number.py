"""
File: 10_largest_odd_number.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Exercise page 20
"""

mylist = [0] * 10
print "enter 10 numbers : "
for i in range(10):
    mylist[i] = int(raw_input('input %s : ' % (i+1,)))
    # the comma is there to remove ambiguity (said someone from IRC)
print mylist
result = 0
for i in range(len(mylist)):
    if mylist[i] % 2 != 0 and mylist[i] > result:
        result = mylist[i]
print 'THe largest odd number of the 10 numbers is %s' % result
