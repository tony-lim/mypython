"""
File: largest_odd_number.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Page 16
"""

x = int(raw_input("Insert integer x : "))
y = int(raw_input("Insert integer y : "))
z = int(raw_input("Insert integer z : "))

# this program examine which is largest, but it has to be odd
if x % 2 != 0:
    largest = x
if y % 2 != 0 and y > largest:
    largest = y
if z % 2 != 0 and z > largest:
    largest = z

if largest != None:
    print "Largest odd number is", largest
else:
    print "All numbers are even"
