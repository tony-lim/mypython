def FancyDivide(numbers, index):
    try:
        denom = numbers[index]
        for i in range(len(numbers)):
            numbers[i] /= denom
    except IndexError:
        FancyDivide(numbers, len(numbers) - 1)
    except ZeroDivisionError:
        print "-2"
    except TypeError:
        print 'Type error dude'
    else:
        print "1"
    finally:
        print '0'

FancyDivide([2, 3, 4], 1)
FancyDivide(['2', '3', '4'], 1)


def FancyDivide2(numbers, index):
    try:
        try:
            denom = numbers[index]
            for i in range(len(numbers)):
                numbers[i] /= denom
        except IndexError, e:
            FancyDivide2(numbers, len(numbers) - 1)
            print str(e)
            # raise IndexError("Test to see what error is this  : " + str(e))
        else:
            print "1"
        finally:
            print "0"
    except ZeroDivisionError, e:
        print "-2" + str(e)

print '------------'
FancyDivide([0, 2, 4], 1)
print '------------'
FancyDivide2([0, 2, 4], 4)


def FancyDivide3(list_of_numbers, index):
    try:
        try:
            raise Exception("asdasd")
        except Exception, e:
            print e
        finally:
            denom = list_of_numbers[index]
            for i in range(len(list_of_numbers)):
                list_of_numbers[i] /= denom
    except Exception, e:
        print e


print '------------'
FancyDivide3([0, 2, 4], 0)


def FancyDivide4(list_of_numbers, index):
    try:
        try:
            denom = list_of_numbers[index]
            for i in range(len(list_of_numbers)):
                list_of_numbers[i] /= denom
        finally:
            raise Exception("asdsad")
    except Exception, e:
        print e
    finally:
        print e

print '---------'
FancyDivide4([0, 2, 4], 0)
