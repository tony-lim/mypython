from ps4a import *
import time

# Problem #6: Computer chooses a word

def isWordComb(word, hand):
    """ This is just a copy of isValidWord """
    # test for number of letters in wordkk
    setInWord = "".join(set(word))
    for letter in setInWord:  # the letter must exists in the hand
        if letter in hand:
            # compare the number of letter in word and in hand
            if word.count(letter) > hand[letter]:
                return False
        else:
            return False
    return True


def compChooseWord(hand, wordList, n):
    """
    Given a hand and a wordList, find the word that gives
    the maximum value score, and return it.

    This word should be calculated by considering all the words
    in the wordList.

    If no words in the wordList can be made from the hand, return None.

    hand: dictionary (string -> int)
    wordList: list (string)
    n: integer (HAND_SIZE; i.e., hand size required for additional points)

    returns: string or None
    """
    # Create a new variable to store the maximum score seen so far (initially 0)
    max_score = 0

    # Create a new variable to store the best word seen so far (initially None)
    best_word = None

    # For each word in the wordList
    for word in wordList:
        # If you can construct the word from your hand
        if isWordComb(word, hand):  # you can use isValidWord, but heavy on computation
            score = getWordScore(word, n)
            if score > max_score:  # If the score for that word is higher than your best score
                max_score = score
                best_word = word
    return best_word  # return the best word you found.


#
# Problem #7: Computer plays a hand
#
def compPlayHand(hand, wordList, n):
    """
    Allows the computer to play the given hand, following the same procedure
    as playHand, except instead of the user choosing a word, the computer
    chooses it.

    1) The hand is displayed.
    2) The computer chooses a word.
    3) After every valid word: the word and the score for that word is
    displayed, the remaining letters in the hand are displayed, and the
    computer chooses another word.
    4)  The sum of the word scores is displayed when the hand finishes.
    5)  The hand finishes when the computer has exhausted its possible
    choices (i.e. compChooseWord returns None).

    hand: dictionary (string -> int)
    wordList: list (string)
    n: integer (HAND_SIZE; i.e., hand size required for additional points)
    """
    score = 0
    while calculateHandlen(hand) > 0:  # As long as there are still letters left in the hand:
        print 'Current Hand : ',
        displayHand(hand)  # Display the hand
        word = compChooseWord(word, wordList, n)
        if word == None:
            break  # End the game (break out of the loop)
        else:  # Otherwise (the word is valid):
            wordScore = getWordScore(word, n)
            score += wordScore
            # Tell the user how many points the word earned, and the updated total score, in one line followed by a blank line
            print '"' + word + '"', 'earned', wordScore, 'points. Total:', score, 'points'
            hand = updateHand(hand, word)  # Update the hand
    # Game is over (user entered a '.' or ran out of letters), so tell user the total score
    if len(hand) == 0:
        print 'Run out of letters.',
    else:
        print 'Game over.',
    print 'Total score : ', score


#
# Problem #8: Playing a game
#
def playGame(wordList):
    """
    Allow the user to play an arbitrary number of hands.

    1) Asks the user to input 'n' or 'r' or 'e'.
        * If the user inputs 'e', immediately exit the game.
        * If the user inputs anything that's not 'n', 'r', or 'e', keep asking them again.

    2) Asks the user to input a 'u' or a 'c'.
        * If the user inputs anything that's not 'c' or 'u', keep asking them again.

    3) Switch functionality based on the above choices:
        * If the user inputted 'n', play a new (random) hand.
        * Else, if the user inputted 'r', play the last hand again.

        * If the user inputted 'u', let the user play the game
          with the selected hand, using playHand.
        * If the user inputted 'c', let the computer play the
          game with the selected hand, using compPlayHand.

    4) After the computer or user has played the hand, repeat from step 1

    wordList: list (string)
    """

    hand = None
    done = False
    donePlay = False
    n = HAND_SIZE
    while not done:
        userInput = raw_input("Enter n to deal a new hand, r to replay the last hand, or e to end game: ")
        if userInput == 'n' or userInput == 'r':
            donePlay = False
            if userInput == 'n':
                n = HAND_SIZE
                hand = dealHand(n)
            elif userInput == 'r':
                if hand is None:
                    print "You have not played a hand yet. Please play a new hand first!"
                    donePlay = True
            while not donePlay:
                userInput = raw_input("Enter u to have yourself play, c to have the computer play: ")
                if userInput == 'u' and hand != None:
                    playHand(hand, wordList, n)
                    donePlay= True
                elif userInput == 'c':
                    compPlayHand(hand, wordList, n)
                    donePlay= True
                else:
                    print 'Invalid command.'
        elif userInput == 'e':
                done = True
        else:
            print 'Invalid command.'


#
# Build data structures used for entire session and play game
#
if __name__ == '__main__':
    wordList = loadWords()
    playGame(wordList)


