def divide(x, y):
    try:
        result = x / y
    except ZeroDivisionError, e:
        print "division by zero! " + str(e)
    else:
        print "result is", result
    finally:
        print "executing finally clause"


def divideNew(x, y):
    try:
        result = x / y
    except ZeroDivisionError, e:
        print "division by zero! " + str(e)
    except TypeError:
        divideNew(int(x), int(y))
    else:
        print "result is", result
    finally:
        print "executing finally clause"


def getValIndex(xList, i):
    """docstring for getValIndex"""
    try:
        value = xList[i]
    except IOError, e:
        print 'Operation error dude\n'
    except IndexError, e:
        print 'Index error dude\n'
        raise IndexError('You accessed invalid index' + str(e))
    except:
        raise ValueError("Test" + str(e))
    else:
        return value

A = [2, 4, 5, 6]
print getValIndex(A, 3)
print '\n'
print '\n'
print getValIndex(A, 4)
