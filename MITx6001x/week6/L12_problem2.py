"""
File: L12_problem2.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/atomic
Description: More inheritance practice
"""


class A(object):

    def __init__(self):
        self.a = 1

    def x(self):
        print "A.x"

    def y(self):
        print "A.y"

    def z(self):
        print "A.z"


class B(A):

    def __init__(self):
        A.__init__(self)
        self.a = 2
        self.b = 3

    def y(self):
        print "B.y"

    def z(self):
        print "B.z"


class C(object):

    def __init__(self):
        self.a = 4
        self.c = 5

    def y(self):
        print "C.y"

    def z(self):
        print "C.z"


class D(C, B):  # Note : the order determine the order or init

    def __init__(self):
        C.__init__(self)  # the order here does not matter
        B.__init__(self)  # the order here does not matter
        self.d = 6

    def z(self):
        print "D.z"

testClass = D()
testClass.y()
