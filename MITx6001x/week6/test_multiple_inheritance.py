class First(object):

    def __init__(self):
        super(First, self).__init__()
        print "first"


class Second(First):

    def __init__(self):
        super(Second, self).__init__()
        print "second"


class Third(Second, First):

    def __init__(self):
        super(Third, self).__init__()
        print "that's it"

testClass = Third()


# test returning const value
class testOne(object):

    """docstring for testOne"""

    def __init__(self, arg):
        self.arg = arg


    def returnArg(self):
        return self.arg  # note, for variable, this will return a copy
                         # for list, it will return reference to list

testList = [1, 2, 3, 4]
A = testOne(testList)
Aarg = A.returnArg()
print Aarg
Aarg[2] = 99
print 'Changed Aarg ', Aarg
print 'A.returnArg() : ', A.returnArg()
