#!/usr/bin/env python
# encoding: utf-8

"""
File: prime_generator.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/atomic
Description: Prime number generator
"""


def genPrimes():
    i = 1
    listPrime = []
    while True:
        i += 1
        # check whether the number is prime (% previous != 0)
        if isPrimeCheck(i, listPrime):
            listPrime.append(i)
            yield i


def isPrimeCheck(i, listPrime):
    for item in listPrime:
        if i % item == 0:
            return False
    return True

for item in genPrimes():
    print item
