#!/usr/bin/python
def genFib(fib0, fib1, fibmax):
    yield fib0
    yield fib1
    while True:
        fibnext = fib0 + fib1
        if(fibnext > fibmax):
            break
        yield fibnext
        fib1, fib0 = fibnext, fib1


print '-'.join([str(i) for i in genFib(0, 1, 100000)])
