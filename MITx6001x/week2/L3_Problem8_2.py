x = 25.0
epsilon = 0.01
step = 0.1
guess = 0.0

while guess <= x:
    if abs(guess**2 - x) >= epsilon:
        guess += step
    #print 'guess : ', guess
    #print 'x : ', x
    print 'guess**2(%f) - x : %f' % (guess**2, guess**2 - x),  raw_input()

if abs(guess**2 - x) >= epsilon:
    print 'failed'
else:
    print 'succeeded: ' + str(guess)
