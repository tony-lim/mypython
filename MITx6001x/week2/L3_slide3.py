# lecture 3.3, slide 3

x = int(raw_input('Enter an integer: '))
print 'you choose the x to be', x
for ans in range(0, abs(x)+1):
    # in python , you delcare ans as new variable
    print ans
    if ans**3 == abs(x):
        break
print 'after the for loop, ans :', ans
if ans**3 != abs(x):
    print(str(x) + ' is not a perfect cube')
else:
    if x < 0:
        ans = -ans
    print('Cube root of ' + str(x) + ' is ' + str(ans))
