"""
File: L3_Problem9.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: For MIT Class
Graded: Correct
"""

print 'Please think of a number between 0 and 100'
low = 0
high = 100
while True:
    ans = (low + high) / 2
    print 'Is your secret number ' + str(ans) + '?'
    reply = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ")
    if reply == 'c':
        break
    elif reply == 'l':
        low = ans
    elif reply == 'h':
        high = ans
    else:
        print 'Sorry, I did not understand your input.'
print 'Game over. Your secret number was:', ans
