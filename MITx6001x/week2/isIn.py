def isIn(string1, string2):
    """Exercise page 36"""
    if string1 in string2:
        return True
    elif string2 in string1:
        return True
    else:
        return False

string1 = raw_input("Input string 1:")
string2 = raw_input("Input string 2:")
isinIsTrue = isIn(string1,string2)
if isinIsTrue:
    print 'yes, isIn is true'
else:
    print 'no, isIn is not true'
