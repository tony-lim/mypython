
"""
File: using_bisection_search.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: You'll notice that in Problem 2, your monthly payment had to be a multiple of $10.
Why did we make it that way? You can try running your code locally so that the payment can be any dollar and cent amount
    (in other words, the monthly payment is a multiple of $0.01).
Does your code still work? It should, but you may notice that your code runs more slowly, especially in cases with very large balances and interest rates.
(Note: when your code is running on our servers, there are limits on the amount of computing time each submission is allowed, so your observations from running this experiment on the grading system might be limited to an error message complaining about too much time taken.)
"""


def findRemainingBalance(balance, annualInterestRate, monthlyPaymentRate):
    """This will try to return the balance of the debt by the end of one year"""
    for item in xrange(1, 13):
        unpaid = balance - monthlyPaymentRate
        interest = annualInterestRate/12*unpaid
        # print item, balance, min_pay, unpaid, interest
        balance = unpaid + interest
    # print 'with monthly ', monthlyPaymentRate
    # print 'The remaining balance by the end of the year is ', round(balance, 2)
    return round(balance, 2)


def totaldebtinayear(balance, annualinterestrate):
    return (balance * (1 + annualInterestRate/12.0)**12.0)


# uncomment this later
balance = float(raw_input('input balance : '))
annualInterestRate = float(raw_input('input annual interest rate : '))

lowerbound = balance/12.0  # this is the case if the interest rate is 0 %
upperbound = totaldebtinayear(balance, annualInterestRate)/12.0

print 'lower bound : ', lowerbound
print 'upper bound : ', upperbound

# bisection search
guess = (lowerbound + upperbound) / 2.0
result = findRemainingBalance(balance, annualInterestRate, guess)
while(abs(result) > 0.01):
    if result > 0:
        lowerbound = guess
    else:
        upperbound = guess
    guess = (lowerbound + upperbound) / 2.0
    print 'result : ', result
    print 'new guess : ', guess
    raw_input()
    result = findRemainingBalance(balance, annualInterestRate, guess)

print 'Lowest Payment: ', round(guess, 2)
