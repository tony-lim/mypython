"""
File: paying_the_minimum.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Problem 1
"""

balance = float(raw_input('input balance : '))
annualInterestRate = float(raw_input('input annual interest rate : '))
monthlyPaymentRate = float(raw_input('input monthly rate : '))

print annualInterestRate, monthlyPaymentRate
# each time the owner pay, interest is added to unpaid balance
# let's assume each month the owner only pay minimum (.02 * balance)

# copy from here to end
total_paid = 0.0
for item in xrange(1, 13):
    min_pay = balance*monthlyPaymentRate
    total_paid += min_pay
    unpaid = balance - min_pay
    interest = annualInterestRate/12*unpaid
    # print item, balance, min_pay, unpaid, interest
    balance = unpaid + interest
    print 'Month: ', item
    print 'Minimum monthly payment: ', round(min_pay, 2)
    print 'Remaining balance: ', round(balance, 2)

print 'Total paid: ', round(total_paid, 2)
print 'Remaining balance: ', round(balance, 2)


# passed
