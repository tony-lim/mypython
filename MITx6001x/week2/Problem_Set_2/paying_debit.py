"""
File: paying_debit.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Just plugging in formulas from the question
"""

balance = float(5000)
annualInterestRate = 0.18
monthlyPaymentRate = 0.02

print annualInterestRate, monthlyPaymentRate
# each time the owner pay, interest is added to unpaid balance
# let's assume each month the owner only pay minimum (.02 * balance)

print '{:>12}  {:>12} {:>12} {:>12} {:>12}'.format('Month', 'balance', 'Minnimum', 'Unpaid', 'Interest')
for item in xrange(5):
    min_pay = balance*monthlyPaymentRate
    unpaid = balance - min_pay
    interest = annualInterestRate/12*unpaid
    # print item, balance, min_pay, unpaid, interest
    print '{:>12}  {:>12}  {:>12} {:>12} {:>12} '.format(item, round(balance, 2), round(min_pay, 2), round(unpaid, 2), round(interest, 2))
    balance = unpaid + interest
