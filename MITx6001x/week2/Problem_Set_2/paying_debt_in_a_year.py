"""
File: paying_debt_in_a_year.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Harder problem
"""


def findRemainingBalance(balance, annualInterestRate, monthlyPaymentRate):
    """This will try to return the balance of the debt by the end of one year"""
    for item in xrange(1, 13):
        unpaid = balance - monthlyPaymentRate
        interest = annualInterestRate/12*unpaid
        # print item, balance, min_pay, unpaid, interest
        balance = unpaid + interest
    # print 'with monthly ', monthlyPaymentRate
    # print 'The remaining balance by the end of the year is ', round(balance, 2)
    return round(balance, 2)


# balance = float(raw_input('input balance : '))
# annualInterestRate = float(raw_input('input annual interest rate : '))

monthly = 10.0
while(findRemainingBalance(balance, annualInterestRate, monthly) > 0):
    monthly += 10.0

print 'Lowest Payment: ', int(monthly)


# checked, passed
