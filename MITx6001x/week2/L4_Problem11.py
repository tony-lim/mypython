def isVowel2(char):
    '''
    Define a function isVowel2(char) that returns True if char is a
    vowel ('a', 'e', 'i', 'o', or 'u'), and False otherwise.
    You can assume that char is a single letter of any case (ie, 'A' and 'a' are both valid).

    This function is similar to the previous problem - but this time, do use the keyword in.
    Your function should take in a single string and return a boolean.

    char: a single letter of any case

    returns: True if char is a vowel and False otherwise.
    '''
    for item in ('A', 'I', 'U', 'E', 'O'):
        if item in char:
           return True
        elif item.lower() in char:
           return True
    return False


# A shorter solution
def isVowel2(char):
    '''
    char: a single letter of any case

    returns: True if char is a vowel and False otherwise.
    '''
    return char.lower() in 'aeiou'
