"""
File: L4_Problem3.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: For Lecture 4 Problem 3 MIT6.00x
"""

def square(x):
    '''
    x: int or float.
    '''
    # your code here
    return x*x
