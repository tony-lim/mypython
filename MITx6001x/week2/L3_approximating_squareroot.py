# lecture 3.5, slide 2

x = float(raw_input('x : '))
epsilon = 0.01
epsilon = float(raw_input('epsilon : '))
#step = epsilon**2
step = float(raw_input('step : '))
numGuesses = 0
ans = 0.0
while (abs(ans**2 - x)) >= epsilon and ans <= x:
    ans += step
    print ans
    numGuesses += 1
print('numGuesses = ' + str(numGuesses))
if abs(ans**2-x) >= epsilon:
    print('Failed on square root of ' + str(x))
else:
    print(str(ans) + ' is close to the square root of ' + str(x))
