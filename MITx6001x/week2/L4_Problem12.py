str1 = 'exterminate!'
str2 = 'number one - the larch!'
print str1.upper()
print str1.isupper()
print str2.capitalize()
print str2.swapcase()
print str2.index('n')  # gives value error when not found
print str2.find('n')
print str2.index('!')
print str2.find('!')
print str1.count('e')
