
def evalQuadratic(a, b, c, x):
    '''
    Write a Python function, evalQuadratic(a, b, c, x),
        that returns the value of the quadratic a⋅x2+b⋅x+c.
    a, b, c: numerical values for the coefficients of a quadratic equation
    x: numerical value at which to evaluate the quadratic.
    '''
    # Your code here
    return a*x*x + b*x + c
