def isVowel(char):
    '''
    Define a function isVowel(char) that returns True if char is a vowel
    ('a', 'e', 'i', 'o', or 'u'), and False otherwise.
    You can assume that char is a single letter of any case (ie, 'A' and 'a' are both valid).

    Do not use the keyword in. Your function should take in a single string and return a boolean.
    char: a single letter of any case

    returns: True if char is a vowel and False otherwise.
    '''
    # Your code here
    for item in ('A', 'I', 'U', 'E', 'O'):
        if char == item:
           return True
        elif char == item.lower():
           return True
    return False


print isVowel('a')
print isVowel('i')
print isVowel('u')
print isVowel('e')
print isVowel('o')
print isVowel('O')
print isVowel('H')
print isVowel('B')
