"""
File: L3_Problem1.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: runtime error
"""

numberOfLoops = 0
numberOfApples = 2
while numberOfLoops < 10:
    numberOfApples *= 2
    numberOfApples += numberOfLoops
    numberOfLoops -= 1  # theres runtime error here
    print numberOfLoops,
    again = raw_input("next ? ")
    if again not in 'yesYES':
        break
print "Number of apples: " + str(numberOfApples)
