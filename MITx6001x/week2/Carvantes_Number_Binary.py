"""
File: Carvantes_Number_Binary.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Practice making binary conversion
"""

x = int(raw_input("Please input your number to convert : "))
print 'Your number : ', x
binary_string = ''

# Wrong, well, kinda
# use X > 0, not x > 2, you can, but its unnecessary
while x > 0:
    binary_string += str(x % 2)
    x /= 2
    print 'binary string : ', binary_string, '; shifted x : ', x


binary_string = binary_string[::-1]
print "So, the binary conversion result is ", binary_string

# Let's try to convert it back to number
converted_integer = 0
for i, value in enumerate(binary_string[::-1]):
    converted_integer += int(value)*2**i
print 'Converted back to integer : ', converted_integer
