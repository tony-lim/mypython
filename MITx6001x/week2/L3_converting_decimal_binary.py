"""
File: L3_converting_decimal_binary.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
"""
num = int(raw_input("Insert the decimal to convert : "))
if num < 0:
    isNeg = True
    num = abs(num)
else:
    isNeg = False
result = ''
if num == 0:
    result = '0'
while num > 0:
    result = str(num % 2) + result
    num = num/2
if isNeg:
    result = '-' + result
print result
