"""
File: L3_Problem4.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Checking if exercises answer make sense(excuse to cheat)
"""

for variable in range(20):
    if variable % 4 == 0:
        print variable
    if variable % 16 == 0:
        print 'Foo!'


count = 0
for letter in 'Snow!':
    print 'Letter # ' + str(count) + ' is ' + str(letter)
    count += 1
    break
print count

var = 10
for var in range(5):
    print var
print var
