"""
File: alphabetical_substring.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description:

Assume s is a string of lower case characters.



Write a program that prints the longest substring of s in which the letters occur in alphabetical
order.
    For example, if s = 'azcbobobegghakl', then your program should print

Longest substring in alphabetical order is: beggh
In the case of ties, print the first substring.
    For example, if s = 'abcbcd', then your program should print

    Longest substring in alphabetical order is: abc

For problems such as these, do not include raw_input statements or define the variable s in any way.
Our automating testing will provide a value of s for you -
        so the code you submit in the following box
should assume s is already defined. If you are confused by this instruction,
please review L4 Problems 10 and 11 before you begin this problem set.

Note: This problem is fairly challenging. We encourage you to work smart.
If you've spent more than a few hours on this problem, we suggest that you move on to a different
part of the course. If you have time, come back to this problem after you've had a break and
cleared your head.

"""


def nextAscSub(start, string):
    """
    function takes in a starting pos and a string
    using python ord() function to generate the next ascending substring
    """
    done = False
    i = start
    while not done:
        print i, string[i]
        if i+1 == len(string):
            print 'String reach ends'
            done = True
        elif ord(string[i]) <= ord(string[i+1]):
            i += 1
        else:
            print 'done becomes true'
            done = True
    if i == start and i+1 != len(string):
        i += 1
    return i


s = 'azcbobobegghakl'
test = 'abca'
test1 = 'dabca'

pos = 0
temp = 0
longest = 0
while(pos < len(s) - 1):
    temp = pos
    pos = nextAscSub(pos, s)
    if longest < (pos - temp):
        longest = pos - temp
    raw_input()
print 'longest : ', longest
