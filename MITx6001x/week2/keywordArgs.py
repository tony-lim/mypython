"""
File: keywordArgs.py
Author: Tony Lim
Email: atomictheorist@gmail.com
Github: https://github.com/gunlockz
Description: Just sample practice to find out python function feature
"""

def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
    print "-- This parrot wouldn't", action,
    print "if you put", voltage, "Volts through it."
    print "-- Lovely plumage, the", type
    print "-- It's", state, "!"

parrot(123)
print '-------------------------------------------'
print '-------------------------------------------'
parrot(10, type = 'Gryphon')
print '-------------------------------------------'
print '-------------------------------------------'
parrot(state='Wretched', type='etc',voltage=500)
