x = 25
epsilon = 0.01
step = 0.1
guess = 0.0

while guess <= x:
    print 'guess^2 = ', guess**2
    if abs(guess**2 - x) < epsilon:
        break
    else:
        guess += step
        print 'guess increased by', guess

if abs(guess**2 - x) >= epsilon:
    print 'failed'
else:
    print 'succeeded: ' + str(guess)
