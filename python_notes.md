
# Python String Formats:

    d	Signed integer decimal.	
    i	Signed integer decimal.	
    o	Unsigned octal.	(1)
    u	Unsigned decimal.	
    x	Unsigned hexidecimal (lowercase).	(2)
    X	Unsigned hexidecimal (uppercase).	(2)
    e	Floating point exponential format (lowercase).	
    E	Floating point exponential format (uppercase).	
    f	Floating point decimal format.	
    F	Floating point decimal format.	
    g	Same as "e" if exponent is greater than -4 or less than precision, "f" otherwise.	
    G	Same as "E" if exponent is greater than -4 or less than precision, "F" otherwise.	
    c	Single character (accepts integer or single character string).	
    r	String (converts any python object using repr()).	(3)
    s	String (converts any python object using str()).	(4)
    %	No argument is converted, results in a "%" character in the result.

# Exception Hierarchy

BaseException
 +-- SystemExit
 +-- KeyboardInterrupt
 +-- GeneratorExit
 +-- Exception
      +-- StopIteration
      +-- StandardError
      |    +-- BufferError
      |    +-- ArithmeticError
      |    |    +-- FloatingPointError
      |    |    +-- OverflowError
      |    |    +-- ZeroDivisionError
      |    +-- AssertionError
      |    +-- AttributeError
      |    +-- EnvironmentError
      |    |    +-- IOError
      |    |    +-- OSError
      |    |         +-- WindowsError (Windows)
      |    |         +-- VMSError (VMS)
      |    +-- EOFError
      |    +-- ImportError
      |    +-- LookupError
      |    |    +-- IndexError
      |    |    +-- KeyError
      |    +-- MemoryError
      |    +-- NameError
      |    |    +-- UnboundLocalError
      |    +-- ReferenceError
      |    +-- RuntimeError
      |    |    +-- NotImplementedError
      |    +-- SyntaxError
      |    |    +-- IndentationError
      |    |         +-- TabError
      |    +-- SystemError
      |    +-- TypeError
      |    +-- ValueError
      |         +-- UnicodeError
      |              +-- UnicodeDecodeError
      |              +-- UnicodeEncodeError
      |              +-- UnicodeTranslateError
      +-- Warning
           +-- DeprecationWarning
           +-- PendingDeprecationWarning
           +-- RuntimeWarning
           +-- SyntaxWarning
           +-- UserWarning
           +-- FutureWarning
	   +-- ImportWarning
	   +-- UnicodeWarning
	   +-- BytesWarning

# Notes:

(1)The alternate form causes a leading zero ("0") to be inserted between left-hand padding and the formatting of the number if the leading character of the result is not already a zero.
(2)The alternate form causes a leading '0x' or '0X' (depending on whether the "x" or "X" format was used) to be inserted between left-hand padding and the formatting of the number if the leading character of the result is not already a zero.
(3)The %r conversion was added in Python 2.0.
(4)If the object or format provided is a unicode string, the resulting string will also be unicode.
Since Python strings have an explicit length, %s conversions do not assume that '\0' is the end of the string.

For safety reasons, floating point precisions are clipped to 50; %f conversions for numbers whose absolute value is over 1e25 are replaced by %g conversions.2.9 All other errors raise exceptions.
Additional string operations are defined in standard modules string and re.

# List of operations and their equivalent functions

    OPERATOR				SYNTAX					FUNCTION
    Addition				a + b					add(a, b)
    Concatenation			seq1 + seq2				concat(seq1, seq2)
    Containment Test		obj in seq				contains(seq, obj)
    Division				a / b					div(a, b) (without __future__.division)
    Division				a / b					truediv(a, b) (with __future__.division)
    Division				a // b					floordiv(a, b)
    Bitwise And				a & b					and_(a, b)
    Bitwise Exclusive Or	a ^ b					xor(a, b)
    Bitwise Inversion		~ a						invert(a)
    Bitwise Or				a | b					Or_(a, b)
    Exponentiation			a ** b					pow(a, b)
    Identity				a is b					is_(a, b)
    Identity				a is not b				is_not(a, b)
    Indexed Assignment		obj[k] = v				setitem(obj, k, v)
    Indexed Deletion		del obj[k]				delitem(obj, k)
    Indexing				obj[k]					getitem(obj, k)
    Left Shift				a << b					lshift(a, b)
    Modulo					a % b					mod(a, b)
    Multiplication			a * b					mul(a, b)
    Negation (Arithmetic)	- a						neg(a)
    Negation (Logical)		not a					not_(a)
    Positive				+ a						pos(a)
    Right Shift				a >> b					rshift(a, b)
    Sequence Repetition		seq * i					repeat(seq, i)
    Slice Assignment		seq[i:j] = values		setitem(seq, slice(i, j), values)
    Slice Deletion			del seq[i:j]			delitem(seq, slice(i, j))
    Slicing					seq[i:j]				getitem(seq, slice(i, j))
    String Formatting		s % obj					mod(s, obj)
    Subtraction				a - b					sub(a, b)
    Truth Test				obj						truth(obj)
    Ordering				a < b					lt(a, b)
    Ordering				a <= b					le(a, b)
    Equality				a == b					eq(a, b)
    Difference				a != b					ne(a, b)
    Ordering				a >= b					ge(a, b)
    Ordering				a > b					gt(a, b)


# COMMANDS AVAILABLE FOR LISTS:
	
list.append(x)		
					list = [1,2]
					list.append([3,4,5]) - > print list :  [ 1 , 2 , [3,4,5] ]

list.extend(L)		Extend the list by appending all the items in the given list; equivalent to a[len(a):] = L.

					list = [1,2]
					lists.extend([3,4,5]) - > print list : [1, 2, 3, 4, 5]

list.insert(i, x)	
					>>> list.insert(2, "10") 
					>>> print list [1,2,10]   

list.remove(x) 		Remove the first item from the list whose value is x. It is an error if there is no such item.
list.pop([i])

					list = [1,3,4,100]
					>>> list.pop() 		-> 100
					>>> list 			-> [1,3,4]
					>>> list.pop(1) 	-> 3
					>>> list 			-> [1,4]

del list[#] 		delete an item from the list. (unlike list.pop, del does not show what item deleted.)
del list[#:#]		delete a list of item from the list. ex: del list[3:5]

list.index(x)  Return the index in the list of the first item whose value is x. It is an error if there is no such item.
	
					list = [1,2,3,4,5]
					>>> list.index(4)  -> 3
					>>> list.index(5)	-> 4	

list.count(x)		Return the number of times x appears in the list.
					list = [1,3,3,3,3,5]
					>>> list.count(3)	-> 4

list.sort()			Sort the items of the list, in place.
list.reverse()		Reverse the elements of the list, in place.

# Tuple:
* The value of tuple can't change.
* can be converted to list with List = list(Tuple)
* can convert list into tuple   tuple = tuple(List)
