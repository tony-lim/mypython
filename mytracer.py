# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2016 Tony Lim <atomictheorist@gmail.com>
#
# Distributed under terms of the MIT license.


from termcolor import colored

# This tracer require nerdfonts on terminal, else, just replace the symbol with
# other arrow
class traced(object):

    color = [ 'grey', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white' ]
    n = len(color)

    """
    traced decorator prints the recursive tree of a function and its return value across
    multiple recursion calls
    """
    level = 0

    def __init__(self, f):
        # replace this and fill in the rest of the class
        self.__name__ = f.__name__
        self.f = f

    def __call__(self, *args, **kwargs):
        try:
            for i in range(traced.level):
                print colored('| ', traced.color[i % traced.n]),
            print ' ' + \
                colored(self.__name__, traced.color[traced.level % traced.n] , attrs=['bold']) + \
                colored(
                  '(' + ', '.join([str(x) for x in args]) + \
                  ', '.join(['{0}={1}'.format(str(x), str(y)) for x, y in kwargs.items()]) + ')'
                    , traced.color[traced.level % traced.n])
            traced.level += 1
            rv = self.f(*args, **kwargs)
            traced.level -= 1
            for i in range(traced.level):
                print colored('| ', traced.color[i % traced.n]),
            print '- ' + colored( str(rv), traced.color[traced.level % traced.n])
            return rv
        except:
            traced.level -= 1
            raise

